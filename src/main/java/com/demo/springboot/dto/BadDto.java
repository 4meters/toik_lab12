package com.demo.springboot.dto;

public class BadDto {

    private String lineIds;
    private String kolumnIds;
    private String areaIds;

    public BadDto(String lineIds, String kolumnIds, String areaIds) {
        this.lineIds = lineIds;
        this.kolumnIds = kolumnIds;
        this.areaIds = areaIds;
    }

    public BadDto() {
    }

    public String getLineIds() {
        return lineIds;
    }

    public String getKolumnIds() {
        return kolumnIds;
    }

    public String getAreaIds() {
        return areaIds;
    }
}

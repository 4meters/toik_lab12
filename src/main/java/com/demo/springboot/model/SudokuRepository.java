package com.demo.springboot.model;

public class SudokuRepository {
    private int sudokuTable[][];

    public SudokuRepository() {
        this.sudokuTable = new SudokuCSVReadComponent().readCSV();
    }

    public int[][] getSudokuTable() {
        return sudokuTable;
    }

}

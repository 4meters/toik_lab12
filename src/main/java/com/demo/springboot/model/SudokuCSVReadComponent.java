package com.demo.springboot.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class SudokuCSVReadComponent {

    public int[][] readCSV() {
        int sudokuTable[][]=new int[9][9];
        String csvFile = "sudoku.csv";
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";
        String[] lineSplit;

        try {
            br = new BufferedReader(new FileReader(csvFile));
            int i=0;
            while ((line = br.readLine()) != null) {
                int j=0;
                lineSplit=line.split(cvsSplitBy);
                for(String item : lineSplit){
                    sudokuTable[i][j]=Integer.parseInt(item);
                    j++;
                }
                i++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sudokuTable;
    }
}

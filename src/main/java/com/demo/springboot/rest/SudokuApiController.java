package com.demo.springboot.rest;

import com.demo.springboot.dto.BadDto;
import com.demo.springboot.model.SudokuCheckService;
import com.demo.springboot.model.SudokuRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value="/api")
public class SudokuApiController {

    private SudokuCheckService sudokuCheckService;

    public SudokuApiController() {

    }

    @PostMapping("/sudoku/verify")
    public ResponseEntity<BadDto> getMovies() {
        sudokuCheckService =new SudokuCheckService();
        if(sudokuCheckService.check()){
            SudokuRepository sudokuRepository=new SudokuRepository();
            return ResponseEntity.ok().body(null);
        }
        else{
            return ResponseEntity.badRequest().body(sudokuCheckService.getBadDto());
        }

    }


}
